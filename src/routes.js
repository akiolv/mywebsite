import React from 'react';
import Nav from './Nav';
import Contact from "./Contact";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import MyAppBar from "./components/myAppBar/MyAppBar";

function Routes() {
    return (
        <Router>
            <MyAppBar />
            <div className="router">
                <Switch>
                    <Route path="/" exact component={Nav} />
                    <Route path="/contact" component={Contact} />
                </Switch>
            </div>
        </Router>
    );
}

export default Routes;
