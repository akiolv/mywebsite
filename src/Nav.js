import react from 'react';
import {Link} from "react-router-dom";

function Nav() {
    return(
        <nav>
            <h3>Test Routing:</h3>
            <ul>
                <Link to='/About'>
                    <li>About</li>
                </Link>
                <Link to='/Contact'>
                    <li>Contact</li>
                </Link>
            </ul>
        </nav>
    )
}

export default Nav;