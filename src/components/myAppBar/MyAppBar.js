import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import HomeRoundedIcon from '@material-ui/icons/HomeRounded';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import CssBaseline from '@material-ui/core/CssBaseline';
import Slide from "@material-ui/core/Slide";

const useStyles = makeStyles((theme) => ({
    root:{
        marginLeft: theme.spacing(5),
        marginRight: theme.spacing(5),
    },
    title: {
        flexGrow: 1,
    },
    navBar: {

    },
    whiteTitle: {
        color: '#FFFFFF',
    },
    blackTitle:{
        color: '#000000',
    },
    solid: {
        background: 'rgba(255,255,255,1.0)',
    },
    transparent: {
        background: 'rgba(0,0,0,0.0)',
    },
}));

export default function MyAppBar() {
    const classes = useStyles();
    const trigger = useScrollTrigger({threshold: 20});

    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar position={'fixed'} style={{boxShadow: "none"}} className={trigger ? classes.solid : classes.transparent}>
                <div className={classes.root}>
                    <Toolbar className={(trigger ? classes.blackTitle : classes.whiteTitle)}>
                        <Typography variant="h6" className={classes.title}>
                            <HomeRoundedIcon style={{ fontSize: 30 }}/>
                        </Typography>
                        <Typography variant="h6" className={'px-3'}>
                            Formations
                        </Typography>
                        <Typography variant="h6" className={'px-3'}>
                            Experiences
                        </Typography>
                        <Typography variant="h6" className={'px-3'}>
                            Project
                        </Typography>
                        <Typography variant="h6" className={'px-3'}>
                            Contact
                        </Typography>
                    </Toolbar>
                </div>
            </AppBar>
        </React.Fragment>
    );
}